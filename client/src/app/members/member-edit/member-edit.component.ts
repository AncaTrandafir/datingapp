import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { Member } from '../../_models/member';
import { User } from '../../_models/user';
import { AccountService } from '../../_services/account.service';
import { MembersService } from '../../_services/members.service';
import { take } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-member-edit',
  templateUrl: './member-edit.component.html',
  styleUrls: ['./member-edit.component.css']
})
export class MemberEditComponent implements OnInit {
  @ViewChild('editForm') editForm: NgForm;
  member: Member;
  user: User;

  // Prevent user accidentally close window by prompting notification.
  @HostListener('window:beforeunload', ['$event']) unloadNotification($event: any) {
    if (this.editForm.dirty) {
      $event.returnValue = true;
    }
  }

  constructor(private accountService: AccountService, private memberService: MembersService,
  private toastrService: ToastrService) {
    this.accountService.currentUser$.pipe(take(1)).subscribe(response => {
      this.user = response;
    })
  }

  ngOnInit(): void {
    this.loadMember();
  }


  loadMember() {
    this.memberService.getMember(this.user.username).subscribe(response => {
      this.member = response;
    })
  }

  updateMember() {
    this.memberService.updateMember(this.member).subscribe( () => {
      this.toastrService.success("Profile updated succcessfully.");
      this.editForm.reset(this.member);
    })
   
  }

}
